    public static void testCase() {
        int n = In.readInt();
        int m = In.readInt();
        
        ArrayList<Integer> l = new ArrayList<Integer>();
        
        Graph g_null = new Graph(n);
        Graph g_all = new Graph(n);
        
        int max_cap = 1; //used later for setting max. capacity of all "-1 roads"
        
        for(int i = 0 ; i < m; i++){
          int u = In.readInt();
          int v = In.readInt();
          int c = In.readInt();
          
          if(c != -1){
            max_cap += c;
            g_all.addEdge(u, v, c);
          }else{
            l.add(u); //need later again
            l.add(v); //need later again
            g_null.addEdge(u, v, 1);
          }
        }
        
        // is there a path of uncloseable roads from s to t
        if(g_null.computeMaximumFlow(0, n-1) > 0){
          Out.println("no");
          return;
        }
        
        //update unclose-abel roads to be able to calculate the max flow (and thus the min-cut)
        for(int i = 0 ; i < l.size() - 1; i = i + 2)
            g_all.addEdge(l.get(i), l.get(i+1), max_cap);

        Out.println(g_all.computeMaximumFlow(0, n-1));
    }
}
