import java.io.*;
import java.util.LinkedList;
import java.util.Scanner;
import java.lang.Math;
import java.lang.Integer;
import java.lang.String;

class Main {
    public static void main(String[] args) {
        // Uncomment the following two lines if you want to read from a file
//        In.open("public/test1.in");
//        Out.compareTo("public/test1.out");

        In.open("public/custom.in");
        Out.compareTo("public/custom.out");
      
        MaxHeap heap = new MaxHeap();
        //
        // Read the number of test cases, and start executing
        //
        int T = In.readInt();
        for (int test = 0; test < T; test += 1) {
          //
          // Read the command from the input
          //
          int command = In.readInt();
          if (command == 1) {
            //
            // If command is '1' then, we must read an array from the input
            // then, build a heap out of the array, using the 'buildHeap'
            // method, and finally, output the heap on the screen.        
            //
            heap.readHeap();
            heap.buildHeap();
            heap.writeHeap();
            
          } else if (command == 2) {
            //
            // if the command is set to '2', then we read the heap values,
            // which already have partial order that satisfies the heap
            // property. Then we read we insert M elements, reading the 
            // M number first.
            //
            heap.readHeap();        
            int M = In.readInt();
            for (int i = 0; i < M; i += 1) {          
              heap.insert(In.readInt());
            }        
            heap.writeHeap();
            
          } else if (command == 3) {
            //
            // if the command is set to '3', then we read the heap values,
            // which already have partial order that satisfies the heap
            // property. Then we delete the maximum elements M times, 
            // after reading the M number first.
            //
            heap.readHeap();        
            int M = In.readInt();;
            for (int i = 0; i < M; i += 1) {
              heap.deleteMax();
            }
            heap.writeHeap();
            
          }
        }
    
        // Uncomment the following line if you want to read from a file
        In.close();
    }

   
}

class MaxHeap {
    //
    // We assume that the heap will not exceed MAX_HEAPSIZE length
    //
    final static int MAX_HEAPSIZE = 100000;    
    //
    // This field describes the number of elements that the heap holds
    //
    private int N = 0;
    //
    // The values of the heap are stored in this array
    //
    private int values[] = new int [MAX_HEAPSIZE];    
    //
    // Default empty constructor
    //
    public MaxHeap () { 
      // do nothing ...
    }    
    //
    // Helper function that provides comparison.
    //
    private boolean cmp(int a, int b) {
      return a < b;
    }
    //
    // Helper function that swaps the i-th & the j-th element of the heap
    //
    private void swap (int i, int j) {
      int tmp = values[i];
      values[i] = values[j];
      values[j] = tmp;
    }
    //
    // The heap will read the values. In this function, 
    // we assume that the values have partial order that satisfies the heap
    // condition.
    //
    public void readHeap () {      
      N = In.readInt();
      for (int i = 0; i < N; i += 1) {
        values[i] = In.readInt();
      }
    }
    //
    // Helper function that is used in printing the state of the heap.
    //
    public void writeHeap () {
      if (N > 0) {
        Out.print(values[0]);
        for (int i = 1; i < N; i += 1) {
          Out.print(" " + values[i]);
        }  
      }    
      Out.println();
    }    
  
    // ====================================================================================================================
    // Complete the methods below. Feel free to add additional methods / fields if needed.
    // ====================================================================================================================
  
    //
    // We assume that values are already stored in the values[] array, but they
    // do not hold the heap condition and have arbitrary order. We need to 
    // restore the heap condition using the method below.
    //
    public static<T> void say(T arg){
      System.out.println(arg);
    }
    
    public static<T> void say(int[] arg){
      System.out.println(java.util.Arrays.toString(arg));
    }

    public void versickern(int i){
        int nachfolger1 = 2 * i + 1;
        int nachfolger2 = 2 * i + 2;
        //say(N + " " + values[i] + "  " + values[nachfolger1] + "  " + values[nachfolger2]);
        if(values[i] < values[nachfolger1] && values[nachfolger2] < values[nachfolger1]){
          swap(i, nachfolger1);
          versickern(nachfolger1);
        }else if(values[i] < values[nachfolger2] && values[nachfolger1] < values[nachfolger2]){
          swap(i, nachfolger2);
          versickern(nachfolger2);
        }
    }

    public void aufsteigen(int i){
        int vorgaenger = (i - 1) / 2;
        if(i > 0 && values[i] > values[vorgaenger]){
          swap(i, vorgaenger);
          aufsteigen(vorgaenger);
        }
    }
    
    public void buildHeap () {
      int start = (N - 2) / 2;
      for(int i = start; i >= 0; i--){
        versickern(i);
      }
    }    
    
    //
    // Inserts a value in the heap, and places it on the right positions such
    // that the heap condition holds.
    //
    public void insert(int value) {             
      values[N++] = value;
      aufsteigen(N-1);
    }
    
    //
    // Pops the first value from the heap, restoring the heap condition
    //
    public void deleteMax () {
      int last = N - 1;
      values[0] = values[last];
      values[last] = 0;
      N--;
      versickern(0);
    }    
    
    // ====================================================================================================================
    // End of implementation
    // ====================================================================================================================
}
