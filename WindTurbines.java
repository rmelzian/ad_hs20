	static int solve(int n, int D, int[] d, int[] e){
      int[] DP = new int[n+1];
	  DP[1] = e[1]; //bei einer Turbine wird gebaut!
	  
	  for(int i = 2; i <= n; i++){ //zweite bis n-te turbine
		int incl = e[i]; //möglicherwiese noch Vorgänger dazu addieren
		int excl = DP[i-1];
		
	    if(d[i] >= D) { //Zusätzliche Turbine nur, wenn Mindestabstand zu Strassenanfang gegeben ist
	    	  int steps = 1;
	    	  while(d[i] - d[i - steps] < D) {steps++;}
	    	  incl = incl + DP[i-steps]; 
	    }	    
	    DP[i] = incl > excl ? incl : excl;
	  }
	  
	  return DP[n];
	}
