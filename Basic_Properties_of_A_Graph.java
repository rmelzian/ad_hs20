  public boolean Reachable(int u, int v)
  {
    boolean isReachable = false;
    LinkedList<Integer> storage = new LinkedList<Integer>();
    boolean[] queued = new boolean[n];

    storage.add(u);
    queued[u] = true;

    while(storage.size() > 0){
      int r = storage.poll();
      if(out_degrees[r] > 0){
          for(int j=0; j < out_degrees[r]; j++){
            if(out_edges[r][j] == v){
              isReachable = true;
              break;
            }
            if(!queued[out_edges[r][j]]){
              queued[out_edges[r][j]] = true;
              storage.add(out_edges[r][j]);
            }
          }
      }
    }
    return isReachable;
  }
