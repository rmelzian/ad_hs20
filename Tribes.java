import algorithms.*;
import java.util.LinkedList;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Arrays;
import java.lang.Math;

class Main {
    public static void main(String[] args) {
        int max_count = 100;
        double[][][][] memory = new double[max_count+1][max_count+1][max_count+1][3];

        // Uncomment this line if you want to read from a file
        //In.open("public/sample.in");
        //Out.compareTo("public/sample.out");
        //In.open("public/custom.in");
        //Out.compareTo("public/custom.out");
        //In.open("public/test1.in");
        //Out.compareTo("public/test1.out");

        int t = In.readInt();
        for (int i = 0; i < t; i++) {
            testCase(memory);
        }
        
        // Uncomment this line if you want to read from a file
        In.close();
    
    }

    public static<T> void say(T arg){
      System.out.println(arg);
    }
    
    public static<T> void say(int[] arg){
      System.out.println(Arrays.toString(arg));
    }

    public static<T> void say(int[][] arg){
      System.out.println(Arrays.deepToString(arg));
    }

    public static<T> void say(Integer[] arg){
      System.out.println(Arrays.toString(arg));
    }

    public static<T> void say(double[] arg){
      System.out.println(Arrays.toString(arg));
    }

    public static<T> void say(double[][] arg){
      System.out.println(Arrays.deepToString(arg));
    }

    public static<T> void say(boolean[][][] arg){
      System.out.println(Arrays.deepToString(arg));
    }
    public static<T> void say(double[][][] arg){
      System.out.println(Arrays.deepToString(arg));
    }
    public static<T> void say(Double[] arg){
      System.out.println(Arrays.toString(arg));
    }

    public static<T> void say(Double[][] arg){
      System.out.println(Arrays.deepToString(arg));
    }
    
    public static<T> void say(Double[][][] arg){
      System.out.println(Arrays.deepToString(arg));
    }

    public static double[] calculate_probability_to_die(int[] population, int[] is_killed_by, boolean[][][] processed, double[][][][] memory) {
      if(processed[population[0]][population[1]][population[2]])
        return memory[population[0]][population[1]][population[2]];
      
      double[] prob = new double[population.length];
      double pr_no_dead = 0.0;
      double omega = 0.0;
    
      for(int i = 0; i < population.length; i++){
        omega += population[i];
      }
      
      if(omega == 1.0){
        return prob;        
      }
      
      for(int i = 0; i < population.length; i++){
        pr_no_dead += population[i] / omega * (population[i] - 1.0) / (omega - 1.0);
      }
    
      for(int i= 0; i < population.length; i++){
        int killer = is_killed_by[i];
        double prob_victim = population[i] / omega;
        double prob_killer_given_victim = prob_victim * population[killer] / (omega - 1.0);
        prob[i] = 2.0 * prob_killer_given_victim / (1.0 - pr_no_dead);
      }

      memory[population[0]][population[1]][population[2]] = prob;
      return prob;
    }

    public static void testCase(double[][][][] memory) {
          final double THRESHOLD = 1e-6;
          
          int B = 0;
          int H = 1;
          int N = 2;
          
          int[] is_killed_by = new int[3];
          is_killed_by[N] = B;
          is_killed_by[B] = H;
          is_killed_by[H] = N;
          
          int[] start_population = {In.readInt(), In.readInt(), In.readInt()};
          
          int bears  = start_population[B];
          int hunter = start_population[H];
          int ninjas = start_population[N];
          
          double[][][] population_probabilities = new double[bears+1][hunter+1][ninjas+1];
          population_probabilities[bears][hunter][ninjas] = 1.0;
          double[] survival_probability = new double[3];
          
          boolean[][][] processed = new boolean[bears+1][hunter+1][ninjas+1];
          
          LinkedList<int[]> queue = new LinkedList<int[]>();
          
          queue.add(start_population);
          
          while( queue.size() > 0) {
            int[] population = queue.removeFirst();
            
            if(processed[population[B]][population[H]][population[N]])
              continue;
              
            double entry_probability = population_probabilities[population[B]][population[H]][population[N]];
            double[] probability_to_die = calculate_probability_to_die(population, is_killed_by, processed, memory);
         
            processed[population[B]][population[H]][population[N]] = true;
           
            for(int i=0; i<population.length; i++){
              
              if( Math.abs(population[i] - 0) < THRESHOLD)
                continue;
              
              if( Math.abs(probability_to_die[i] - 1.0) < THRESHOLD)
                 continue;
            
              if( Math.abs(probability_to_die[i]) < THRESHOLD ){
                survival_probability[i] += entry_probability;
              }else{

                int[] next_population = {population[0], population[1], population[2]};
                next_population[i]--;

                if(processed[next_population[B]][next_population[H]][next_population[N]])
                  continue;
                
                population_probabilities[next_population[B]][next_population[H]][next_population[N]] += probability_to_die[i] * entry_probability;
                queue.add(next_population);
              }
            }
          } 
          Out.println(survival_probability[B] + " " + survival_probability[H] + " " + survival_probability[N]);
        }
}
