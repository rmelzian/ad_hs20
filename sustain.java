    public static void testCase() {
        // Input using In.java class
        int n = In.readInt();
        int m = In.readInt();
        
        int s = n;
        int t = s + 1;
        
        int expected = 0;

        Graph G = new Graph(n + 2);
        
        for(int i = 0 ; i < n; i++){
          int has = In.readInt();
          int needs = In.readInt();
          if(has > 0){
            G.addEdge(s,i,has);
          }
          if(needs > 0){
            G.addEdge(i,t,needs);
            expected += needs;
            say(i + " " + s + " " + t  + " " + has  + " " + needs + " " + expected);
          }
        }
        
        for(int i = 0 ; i < m; i++){
          int u = In.readInt();
          int v = In.readInt();
          if(G.getCapacity(v,t) > 0){
            G.addEdge(u,v,G.getCapacity(s,u));
            say(u + " " + v);
          }
        }
        
        // Output using Out.java class
        int maxf = G.computeMaximumFlow(s,t);
        String answer = maxf == expected ? "yes" : "no";
    
        Out.println(answer);
    }
