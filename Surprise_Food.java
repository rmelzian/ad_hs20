    public static void testCase_defekt() {
        boolean missing_order = false;
        int dishes = In.readInt();
        int clients = In.readInt();
        int s = dishes;
        int t = s + 1;
        
        Graph g = new Graph(t + 1);
        
        for(int j = 0; j < dishes; j++)
           g.addEdge(s, j, In.readInt());

        for(int j = 0; j < clients; j++){
          int k = In.readInt();
          if(k == 0)          
            missing_order = true;

          for(int l = 0; l < k; l++)
             g.addEdge(In.readInt(), t, 1);
        }
        Out.println(g.computeMaximumFlow(s, t) < clients || missing_order ? "no" : "yes");
    }
