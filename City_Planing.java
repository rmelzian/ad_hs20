    public static void testCase() {
        int grid = 1000;
        long[] x = new long[2 * grid + 1];
        long[] y = new long[x.length];
        long d = 0;

        int n = In.readInt();
        for(int i=0; i < n; i++){
          x[In.readInt() + grid]++;
          y[In.readInt() + grid]++;
        }
        
        for(int i=0; i < x.length - 1; i++){
          for(int j=i+1; j < x.length; j++){
              if(x[i] > 0 && x[j] > 0)
                  d += Long.valueOf((j - i)) * x[j] * x[i];
              if(y[i] > 0 && y[j] > 0)
                  d += Long.valueOf((j - i)) * y[j] * y[i];
          }
        }
        Out.println(d);
    }
