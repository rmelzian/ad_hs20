class Main {
    
    public static final double undef = -1.0;
    public static final int MAX_HealthPoints = 501;
    public static final int MAX_FrostPower = 51;
    public static final int MAX_ShadowPower = 51;
    public static double[][][] dp = new double[MAX_HealthPoints][MAX_ShadowPower][MAX_FrostPower];
...
}

public static void testCase() {
      
        int Dragon_Health_Points = In.readInt();
        int Frost_Power_Points = In.readInt();
        int Shadow_Power_Points = In.readInt();
        int Frost_Power = In.readInt();
        int Shadow_Power = In.readInt();
        
        if(Dragon_Health_Points > 
          Frost_Power_Points * Frost_Power + 
          Shadow_Power_Points * Shadow_Power){
            Out.println(-1);
            return;
        }
        
        //init DP table
        for(int i=0; i< dp.length; i++){
          for(int j=0; j< dp[0].length; j++){
              for(int k=0; k < dp[0][0].length; k++){
                dp[i][j][k] = undef;
            }  
          }  
        }
        
        double Expected_Spell_Count = expected_spells(
                                        Dragon_Health_Points, 
                                        Frost_Power_Points, 
                                        Shadow_Power_Points, 
                                        Frost_Power,
                                        Shadow_Power,
                                        0,
                                        1.0);

        Out.println(Expected_Spell_Count);
}

public static double expected_spells(int Dragon_Health_Points, 
                                         int Frost_Power_Points, 
                                         int Shadow_Power_Points, 
                                         int Frost_Power,
                                         int Shadow_Power,
                                         int Spells,
                                         double Probability) {
      
      if( dp[Dragon_Health_Points][Frost_Power_Points][Shadow_Power_Points] != undef )
          return Probability * dp[Dragon_Health_Points][Frost_Power_Points][Shadow_Power_Points];
      
      if( Dragon_Health_Points <= 0)
        return Probability * Double.valueOf(Spells);
      
      double frost = 0.0;
      double shadow = 0.0;
      double prob_frost = Double.valueOf(Frost_Power_Points) / Double.valueOf(Frost_Power_Points + Shadow_Power_Points);
      
      if( Frost_Power_Points > 0){
        frost = expected_spells(Math.max((Dragon_Health_Points - Frost_Power),0), 
                                Frost_Power_Points - 1, 
                                Shadow_Power_Points - 0, 
                                Frost_Power,
                                Shadow_Power,
                                Spells + 1,
                                prob_frost);
      }
      
      if( Shadow_Power_Points > 0){
        shadow = expected_spells(Math.max((Dragon_Health_Points - Shadow_Power),0), 
                                 Frost_Power_Points - 0, 
                                 Shadow_Power_Points - 1, 
                                 Frost_Power,
                                 Shadow_Power,
                                 Spells + 1,
                                 1.0 - prob_frost);
      }
      
      dp[Dragon_Health_Points][Frost_Power_Points][Shadow_Power_Points] = frost + shadow ;
      
      return Probability * (frost + shadow) ;
    }
