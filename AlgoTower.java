    //n bezeichnet die Anzahl der ALGO Steine.
	//l[i] enthält die Länge des i-ten ALGO Steins, für i=1,...,n
	//b[i] enthält die Breite des i-ten ALGO Steins, für i=1,...,n
	//h[i] enthält die Höhe des i-ten ALGO Steins, für i=1,...,n
	static boolean fits(int i, int j, int[] b, int[] l) {
		return (l[i] <= l[j] && b[i] <= b[j]) || (b[i] <= l[j] && l[i] <= b[j]);
	}

	static int solve(int n, int[] l, int[] b, int[] h)
	{
		//TODO: Geben Sie die Höhe des höchsten ALGO Turms, der aus den vorhandenen Steinen
		//gebaut werden kann, zurück.
		int global_max = 0;
        
		//determining the tower height with brick "i" as top brick
		//literally we are trying to put each brick on top off all 
		//bricks that have smaller area (array is sorted)
		int[] DP = new int[n+1];

		for(int i = 1; i < DP.length; i++){
		  int max = 0;
		  for(int j = 1; j < i; j++){
			  if( fits(i,j,b,l) ) {
				  max = java.lang.Math.max(DP[j], max);
			  }
		  }
		  DP[i] = h[i] + max;
	
		  global_max = java.lang.Math.max(global_max, DP[i]);
	    }
        return global_max;
	}
